import { SchemaValidator } from './objectResolver';

/**
 * Configuration schema
 * @author Masoud Zohrabi <mdzzohrabi@gmail.com>
 */
export class ConfigSchema extends SchemaValidator { }