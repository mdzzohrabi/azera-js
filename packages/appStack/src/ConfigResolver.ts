import { ObjectResolver } from './objectResolver';

/**
 * Config resolver
 * @author Masoud Zohrabi <mdzzohrabi@gmail.com>
 */
export class ConfigResolver extends ObjectResolver { }